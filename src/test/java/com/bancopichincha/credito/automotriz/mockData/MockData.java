package com.bancopichincha.credito.automotriz.mockData;

import com.bancopichincha.credito.automotriz.dto.ClienteResponseDto;
import com.bancopichincha.credito.automotriz.model.Cliente;

import java.util.Date;

public class MockData {

    public static Cliente getClienteMock() {
        return Cliente.builder()
                .id(1L)
                .sujetoCredito("SUJETO CREDITO")
                .nombreConyugue("Nombre conyugue ejemplo")
                .identificacionConyugue("identificacion ejemplo")
                .estadoCivil("Estado civil ejemplo")
                .identificacion("Identificacion ejemplo")
                .nombres("Nombres ejemplo")
                .apellidos("Apellidos ejemplo")
                .edad(25)
                .fechaNacimiento(new Date())
                .direccion("Direccion Ejemplo")
                .telefono("Telefono ejemplo")
                .estadoCivil("Estado civil ejemplo")
                .build();
    }

    public static ClienteResponseDto getClienteResponseDtoMock() {
        return ClienteResponseDto.builder()
                .nombreConyugue("Nombre conyugue ejemplo")
                .identificacionConyugue("identificacion ejemplo")
                .estadoCivil("Estado civil ejemplo")
                .identificacion("Identificacion ejemplo")
                .nombres("Nombres ejemplo")
                .apellidos("Apellidos ejemplo")
                .edad(25)
                .fechaNacimiento(new Date())
                .direccion("Direccion Ejemplo")
                .telefono("Telefono ejemplo")
                .estadoCivil("Estado civil ejemplo")
                .build();
    }
}
