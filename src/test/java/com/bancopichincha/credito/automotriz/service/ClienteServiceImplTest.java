package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.ClienteRequestDto;
import com.bancopichincha.credito.automotriz.exception.ClienteNotFoundException;
import com.bancopichincha.credito.automotriz.mapper.ClienteMapper;
import com.bancopichincha.credito.automotriz.mockData.MockData;
import com.bancopichincha.credito.automotriz.model.Cliente;
import com.bancopichincha.credito.automotriz.repository.ClienteRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith({MockitoExtension.class})
class ClienteServiceImplTest {

    @InjectMocks
    private ClienteService clienteService = new ClienteServiceImpl();

    @Spy
    private ClienteMapper clienteMapper;

    @Spy
    private ClienteRepository clienteRepository;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldReturnClientNotFoundException() {
        System.out.println(clienteRepository);
        when(clienteRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(ClienteNotFoundException.class, () -> clienteService.getCliente(0L));
    }

    @Test
    void shouldReturnClient() {
        var expected = MockData.getClienteMock();
        System.out.println(clienteRepository);
        when(clienteRepository.findById(any(Long.class))).thenReturn(Optional.of(MockData.getClienteMock()));
        var cliente = clienteService.getCliente(1L);
        assertThat(cliente.getId(), Matchers.equalTo(expected.getId()));
        assertThat(cliente.getNombres(), Matchers.equalTo(expected.getNombres()));
        assertThat(cliente.getApellidos(), Matchers.equalTo(expected.getApellidos()));
        assertThat(cliente.getDireccion(), Matchers.equalTo(expected.getDireccion()));
        assertThat(cliente.getEstadoCivil(), Matchers.equalTo(expected.getEstadoCivil()));
        assertThat(cliente.getDireccion(), Matchers.equalTo(expected.getDireccion()));
        assertThat(cliente.getIdentificacion(), Matchers.equalTo(expected.getIdentificacion()));
        assertThat(cliente.getNombreConyugue(), Matchers.equalTo(expected.getNombreConyugue()));
    }

//    @Test
//    void shouldReturnClientAsResponse() {
//        System.out.println(clienteRepository);
//        var expected = MockData.getClienteResponseDtoMock();
//        when(clienteRepository.findById(any(Long.class))).thenReturn(Optional.of(MockData.getClienteMock()));
//        var clienteResponse = clienteService.getClienteAsResponse(1L);
//        assertThat(clienteResponse.getNombres(), Matchers.equalTo(expected.getNombres()));
//        assertThat(clienteResponse.getApellidos(), Matchers.equalTo(expected.getApellidos()));
//        assertThat(clienteResponse.getDireccion(), Matchers.equalTo(expected.getDireccion()));
//        assertThat(clienteResponse.getEstadoCivil(), Matchers.equalTo(expected.getEstadoCivil()));
//        assertThat(clienteResponse.getDireccion(), Matchers.equalTo(expected.getDireccion()));
//        assertThat(clienteResponse.getIdentificacion(), Matchers.equalTo(expected.getIdentificacion()));
//        assertThat(clienteResponse.getNombreConyugue(), Matchers.equalTo(expected.getNombreConyugue()));
//    }

//    @Test
//    void shouldModifyCliente() {
//        var expected = MockData.getClienteResponseDtoMock().toBuilder()
//                .apellidos("Updated Apellidos")
//                .build();
//        var clienteUpdated = MockData.getClienteMock().toBuilder()
//                .apellidos("Updated Apellidos")
//                .build();
//        var request = ClienteRequestDto.builder()
//                .apellidos("Updated Apellidos")
//                .build();
//
//        when(clienteRepository.findById(any(Long.class))).thenReturn(Optional.of(MockData.getClienteMock()));
//        when(clienteRepository.save(any(Cliente.class))).thenReturn(clienteUpdated);
//        when(clienteMapper.modelToResponseDTO(any(Cliente.class))).thenReturn(expected);
//
//        var clienteResponse = clienteService.modifyCliente(request, 1L);
//        assertThat(clienteResponse.getNombres(), Matchers.equalTo(expected.getNombres()));
//    }

}
