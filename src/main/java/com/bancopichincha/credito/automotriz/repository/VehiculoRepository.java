package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.model.Vehiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehiculoRepository extends JpaRepository<Vehiculo, Long> {

    List<Vehiculo> findVehiculoByModeloAndMarca(Long modelo, String marca);

}
