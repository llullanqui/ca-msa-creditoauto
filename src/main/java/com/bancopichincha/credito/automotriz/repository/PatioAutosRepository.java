package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.model.PatioAutos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatioAutosRepository extends JpaRepository<PatioAutos, Long> {
}
