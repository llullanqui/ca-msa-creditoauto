package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.model.AsignacionCliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AsignacionClienteRepository extends JpaRepository<AsignacionCliente, Long> {
}
