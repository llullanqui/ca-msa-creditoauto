package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
