package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.model.Ejecutivo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EjecutivoRepository extends JpaRepository<Ejecutivo, Long>  {
}
