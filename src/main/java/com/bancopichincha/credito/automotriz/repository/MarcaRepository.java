package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.model.Marca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarcaRepository extends JpaRepository<Marca, Long> {
}
