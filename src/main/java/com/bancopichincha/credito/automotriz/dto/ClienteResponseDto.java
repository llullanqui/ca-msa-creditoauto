package com.bancopichincha.credito.automotriz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClienteResponseDto {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String identificacion;

    @JsonProperty
    private String nombres;

    @JsonProperty
    private String apellidos;

    @JsonProperty
    private int edad;

    @JsonProperty("fecha_nacimiento")
    private Date fechaNacimiento;

    @JsonProperty
    private String direccion;

    @JsonProperty
    private String telefono;

    @JsonProperty
    private String estadoCivil;

    @JsonProperty("identificacion_conyugue")
    private String identificacionConyugue;

    @JsonProperty("nombre_conyugue")
    private String nombreConyugue;

    @JsonProperty
    private String sujetoCredito;

}
