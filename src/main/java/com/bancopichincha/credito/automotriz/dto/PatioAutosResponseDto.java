package com.bancopichincha.credito.automotriz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PatioAutosResponseDto {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String nombre;

    @JsonProperty
    private String direccion;

    @JsonProperty
    private String telefono;

    @JsonProperty("numero_puntos_venta")
    private int numeroPuntosVenta;

}
