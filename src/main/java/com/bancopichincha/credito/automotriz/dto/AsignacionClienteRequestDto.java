package com.bancopichincha.credito.automotriz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AsignacionClienteRequestDto {

    @JsonProperty
    private Long cliente;

    @JsonProperty
    private Long patio;

}
