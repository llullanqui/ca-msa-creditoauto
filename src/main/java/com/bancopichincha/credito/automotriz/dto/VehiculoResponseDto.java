package com.bancopichincha.credito.automotriz.dto;

import com.bancopichincha.credito.automotriz.model.Marca;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VehiculoResponseDto {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String placa;

    @JsonProperty
    private String modelo;

    @JsonProperty("numero_chasis")
    private String numeroChasis;

    @JsonProperty
    private Marca marca;

    @JsonProperty
    private String tipo;

    @JsonProperty
    private String cilindraje;

    @JsonProperty
    private BigDecimal avaluo;

}
