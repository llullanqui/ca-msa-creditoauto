package com.bancopichincha.credito.automotriz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AsignacionClienteResponseDto {

    @JsonProperty
    private int id;

    @JsonProperty
    private ClienteResponseDto cliente;

    @JsonProperty
    private PatioAutosResponseDto patio;

    @JsonProperty
    private Date fechaAsignacion;

}
