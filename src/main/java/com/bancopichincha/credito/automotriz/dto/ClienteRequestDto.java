package com.bancopichincha.credito.automotriz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClienteRequestDto {

    @JsonProperty
    private String identificacion;

    @JsonProperty
    private String nombres;

    @JsonProperty
    private String apellidos;

    @JsonProperty
    private int edad;

    @JsonProperty
    private Date fechaNacimiento;

    @JsonProperty
    private String direccion;

    @JsonProperty
    private String telefono;

    @JsonProperty
    private String estadoCivil;

    @JsonProperty
    private String identificacionConyugue;

    @JsonProperty
    private String nombreConyugue;

}
