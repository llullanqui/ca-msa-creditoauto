package com.bancopichincha.credito.automotriz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PatioAutosRequestDto {

    @JsonProperty
    private String nombre;

    @JsonProperty
    private String direccion;

    @JsonProperty
    private String telefono;

    @JsonProperty("numero_puntos_venta")
    private int numeroPuntosVenta;

}
