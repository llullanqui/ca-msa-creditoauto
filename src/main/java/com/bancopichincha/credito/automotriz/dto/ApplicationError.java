package com.bancopichincha.credito.automotriz.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@RequiredArgsConstructor
public class ApplicationError {

    private final HttpStatus httpStatus;
    private final String message;

}
