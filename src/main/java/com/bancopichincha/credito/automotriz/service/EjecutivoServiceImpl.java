package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.exception.EjecutivoRepetidoException;
import com.bancopichincha.credito.automotriz.model.Ejecutivo;
import com.bancopichincha.credito.automotriz.repository.EjecutivoRepository;
import com.bancopichincha.credito.automotriz.utils.ArrayConverts;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service("ejecutivo_service")
@NoArgsConstructor
public class EjecutivoServiceImpl implements EjecutivoService, InitialDataService {

    @Autowired
    private EjecutivoRepository ejecutivoRepository;

    public void loadData() throws EjecutivoRepetidoException {
        if (!ejecutivoRepository.findAll().isEmpty()) return;
        List<Ejecutivo> ejecutivos = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new ClassPathResource("data/EJECUTIVOS_DATA.csv").getFile()))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                Ejecutivo ejecutivo = ArrayConverts.buildEjecutivoFromArray(values);
                ejecutivos.add(ejecutivo);
            }
        } catch (Exception e) {
            System.out.println("Unable to save ejecutivos: " + e.getMessage());
        }
        if(ejecutivos.size() != new HashSet<>(ejecutivos).size())
            throw new EjecutivoRepetidoException();
        ejecutivoRepository.saveAll(new HashSet<>(ejecutivos));
    }

}
