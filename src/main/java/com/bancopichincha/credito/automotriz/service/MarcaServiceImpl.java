package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.exception.ClienteNotFoundException;
import com.bancopichincha.credito.automotriz.exception.EjecutivoRepetidoException;
import com.bancopichincha.credito.automotriz.exception.MarcaNotFoundException;
import com.bancopichincha.credito.automotriz.model.Cliente;
import com.bancopichincha.credito.automotriz.model.Marca;
import com.bancopichincha.credito.automotriz.repository.MarcaRepository;
import com.bancopichincha.credito.automotriz.utils.ArrayConverts;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service("marca_service")
@NoArgsConstructor
public class MarcaServiceImpl implements MarcaService, InitialDataService {

    @Autowired
    public MarcaRepository marcaRepository;

    @Override
    @Transactional(readOnly = true)
    public Marca getMarca(Long id) {
        var marca = marcaRepository.findById(id);
        if(marca.isPresent()) return marca.get();
        else throw new MarcaNotFoundException();
    }

    public void loadData() {
        if (!marcaRepository.findAll().isEmpty()) return;
        List<Marca> marcas = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new ClassPathResource("data/MARCAS_DATA.csv").getFile()))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                Marca marca = ArrayConverts.buildMarcaFromArray(values);
                marcas.add(marca);
            }
        } catch (Exception e) {
            System.out.println("Unable to save marcas: " + e.getMessage());
        }
        marcaRepository.saveAll(new HashSet<>(marcas));
    }
}
