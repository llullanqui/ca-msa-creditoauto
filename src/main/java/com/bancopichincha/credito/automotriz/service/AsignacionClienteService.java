package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.AsignacionClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.AsignacionClienteResponseDto;
import com.bancopichincha.credito.automotriz.model.AsignacionCliente;

public interface AsignacionClienteService {

    AsignacionCliente getAsignacionCliente(Long id);

    AsignacionClienteResponseDto getAsignacionClienteAsResponse(Long id);

    AsignacionClienteResponseDto createAsignacionCliente(AsignacionClienteRequestDto requestDto);

    AsignacionClienteResponseDto modifyAsignacionCliente(AsignacionClienteRequestDto requestDto, Long id);

    void deleteAsignacionCliente(Long id);

}
