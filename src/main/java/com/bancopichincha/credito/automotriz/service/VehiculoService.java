package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.VehiculoRequestDto;
import com.bancopichincha.credito.automotriz.dto.VehiculoResponseDto;
import com.bancopichincha.credito.automotriz.model.Vehiculo;

import java.util.List;

public interface VehiculoService {

    Vehiculo getVehiculo(Long id);

    List<VehiculoResponseDto> getVehiculoByMarcaAndModelo(Long marca, String modelo);

    VehiculoResponseDto getVehiculoAsResponse(Long id);

    VehiculoResponseDto createVehiculo(VehiculoRequestDto requestDto);

    VehiculoResponseDto modifyVehiculo(VehiculoRequestDto requestDto, Long id);

    void deleteVehiculo(Long id);
}
