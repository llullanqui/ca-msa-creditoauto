package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.PatioAutosRequestDto;
import com.bancopichincha.credito.automotriz.dto.PatioAutosResponseDto;
import com.bancopichincha.credito.automotriz.exception.ClienteNotFoundException;
import com.bancopichincha.credito.automotriz.exception.PatioAutosNotFoundException;
import com.bancopichincha.credito.automotriz.mapper.PatioAutosMapper;
import com.bancopichincha.credito.automotriz.model.PatioAutos;
import com.bancopichincha.credito.automotriz.repository.PatioAutosRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("patio_autos_service")
@NoArgsConstructor
public class PatioAutosServiceImpl implements PatioAutosService {

    @Autowired
    private PatioAutosRepository patioAutosRepository;

    @Autowired
    private PatioAutosMapper patioAutosMapper;

    @Override
    @Transactional(readOnly = true)
    public PatioAutos getPatioAutos(Long id) {
        var patioAutos = patioAutosRepository.findById(id);
        if(patioAutos.isPresent()) return patioAutos.get();
        else throw new PatioAutosNotFoundException();
    }

    @Override
    @Transactional(readOnly = true)
    public PatioAutosResponseDto getPatioAutosAsResponse(Long id) {
        return patioAutosMapper.modelToResponseDto(getPatioAutos(id));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PatioAutosResponseDto createPatioAutos(PatioAutosRequestDto requestDto) {
        var patioAutos = patioAutosMapper.requestDtoToModel(requestDto);
        patioAutosRepository.save(patioAutos);
        return patioAutosMapper.modelToResponseDto(patioAutos);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PatioAutosResponseDto modifyPatioAutos(PatioAutosRequestDto requestDto, Long id) {
        var patioAutos = getPatioAutos(id);
        patioAutosMapper.updatePatioAutosFromDto(requestDto, patioAutos);
        return patioAutosMapper.modelToResponseDto(patioAutosRepository.save(patioAutos));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deletePatioAutos(Long id) {
        patioAutosRepository.delete(getPatioAutos(id));
    }
}
