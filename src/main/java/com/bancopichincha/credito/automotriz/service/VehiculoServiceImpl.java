package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.VehiculoRequestDto;
import com.bancopichincha.credito.automotriz.dto.VehiculoResponseDto;
import com.bancopichincha.credito.automotriz.exception.VehiculoNotFoundException;
import com.bancopichincha.credito.automotriz.mapper.VehiculoMapper;
import com.bancopichincha.credito.automotriz.model.Vehiculo;
import com.bancopichincha.credito.automotriz.repository.VehiculoRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service("vehiculo_service")
@NoArgsConstructor
public class VehiculoServiceImpl implements VehiculoService {

    @Autowired
    private VehiculoRepository vehiculoRepository;

    @Autowired
    private VehiculoMapper vehiculoMapper;

    @Override
    @Transactional(readOnly = true)
    public Vehiculo getVehiculo(Long id) {
        var vehiculo = vehiculoRepository.findById(id);
        if(vehiculo.isPresent()) return vehiculo.get();
        else throw new VehiculoNotFoundException();
    }

    @Override
    public List<VehiculoResponseDto> getVehiculoByMarcaAndModelo(Long marca, String modelo) {
        return vehiculoRepository.findVehiculoByModeloAndMarca(marca, modelo).stream()
                .map(vehiculo -> vehiculoMapper.modelToResponseDto(vehiculo))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public VehiculoResponseDto getVehiculoAsResponse(Long id) {
        return vehiculoMapper.modelToResponseDto(getVehiculo(id));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public VehiculoResponseDto createVehiculo(VehiculoRequestDto requestDto) {
        var vehiculo = vehiculoMapper.requestDtoToModel(requestDto);
        vehiculoRepository.save(vehiculo);
        return vehiculoMapper.modelToResponseDto(vehiculo);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public VehiculoResponseDto modifyVehiculo(VehiculoRequestDto requestDto, Long id) {
        var vehiculo = getVehiculo(id);
        vehiculoMapper.updateVehiculoFromDto(requestDto, vehiculo);
        return vehiculoMapper.modelToResponseDto(vehiculoRepository.save(vehiculo));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteVehiculo(Long id) {
        vehiculoRepository.delete(getVehiculo(id));
    }

}
