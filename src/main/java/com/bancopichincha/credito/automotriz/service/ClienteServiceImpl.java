package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.ClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.ClienteResponseDto;
import com.bancopichincha.credito.automotriz.exception.ClienteNotFoundException;
import com.bancopichincha.credito.automotriz.mapper.ClienteMapper;
import com.bancopichincha.credito.automotriz.model.Cliente;
import com.bancopichincha.credito.automotriz.repository.ClienteRepository;
import com.bancopichincha.credito.automotriz.utils.ArrayConverts;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service("cliente_service")
@NoArgsConstructor
public class ClienteServiceImpl implements ClienteService, InitialDataService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteMapper clienteMapper;

    @Override
    @Transactional(readOnly = true)
    public Cliente getCliente(Long id) {
        var cliente = clienteRepository.findById(id);
        if(cliente.isPresent()) return cliente.get();
        else throw new ClienteNotFoundException();
    }

    @Override
    @Transactional(readOnly = true)
    public ClienteResponseDto getClienteAsResponse(Long id) {
        return clienteMapper.modelToResponseDTO(getCliente(id));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ClienteResponseDto createCliente(ClienteRequestDto requestDto) {
        var cliente = clienteMapper.requestDtoToModel(requestDto);
        clienteRepository.save(cliente);
        return clienteMapper.modelToResponseDTO(cliente);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ClienteResponseDto modifyCliente(ClienteRequestDto requestDto, Long id) {
        var cliente = getCliente(id);
        clienteMapper.updateClienteFromDto(requestDto, cliente);
        return clienteMapper.modelToResponseDTO(clienteRepository.save(cliente));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCliente(Long id) {
        clienteRepository.delete(getCliente(id));
    }

    @Override
    public void loadData() {
        if (!clienteRepository.findAll().isEmpty()) return;
        List<Cliente> clientes = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new ClassPathResource("data/CLIENTES_DATA.csv").getFile()))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                Cliente cliente = ArrayConverts.buildClientFromArray(values);
                clientes.add(cliente);
            }
        } catch (Exception e) {
            System.out.println("Unable to save clientes: " + e.getMessage());
        }
        clienteRepository.saveAll(new HashSet<>(clientes));
    }

}
