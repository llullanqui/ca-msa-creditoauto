package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.PatioAutosRequestDto;
import com.bancopichincha.credito.automotriz.dto.PatioAutosResponseDto;
import com.bancopichincha.credito.automotriz.model.PatioAutos;

public interface PatioAutosService {

    PatioAutos getPatioAutos(Long id);

    PatioAutosResponseDto getPatioAutosAsResponse(Long id);

    PatioAutosResponseDto createPatioAutos(PatioAutosRequestDto requestDto);

    PatioAutosResponseDto modifyPatioAutos(PatioAutosRequestDto requestDto, Long id);

    void deletePatioAutos(Long id);

}
