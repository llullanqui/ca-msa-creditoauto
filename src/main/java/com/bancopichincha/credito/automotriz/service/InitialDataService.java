package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.exception.EjecutivoRepetidoException;

public interface InitialDataService {

    void loadData() throws EjecutivoRepetidoException;

}
