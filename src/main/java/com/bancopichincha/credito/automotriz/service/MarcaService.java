package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.model.Marca;

public interface MarcaService {

    Marca getMarca(Long id);

}
