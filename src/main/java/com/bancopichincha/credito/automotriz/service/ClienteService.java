package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.ClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.ClienteResponseDto;
import com.bancopichincha.credito.automotriz.model.Cliente;

public interface ClienteService {

    Cliente getCliente(Long id);

    ClienteResponseDto getClienteAsResponse(Long id);

    ClienteResponseDto createCliente(ClienteRequestDto requestDto);

    ClienteResponseDto modifyCliente(ClienteRequestDto requestDto, Long id);

    void deleteCliente(Long id);

}
