package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.dto.AsignacionClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.AsignacionClienteResponseDto;
import com.bancopichincha.credito.automotriz.exception.AsignacionClienteNotFoundException;
import com.bancopichincha.credito.automotriz.exception.ClienteNotFoundException;
import com.bancopichincha.credito.automotriz.mapper.AsignacionClienteMapper;
import com.bancopichincha.credito.automotriz.mapper.ClienteMapper;
import com.bancopichincha.credito.automotriz.model.AsignacionCliente;
import com.bancopichincha.credito.automotriz.repository.AsignacionClienteRepository;
import com.bancopichincha.credito.automotriz.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("asignacion_cliente_service")
public class AsignacionClienteServiceImpl implements AsignacionClienteService {

    @Autowired
    private AsignacionClienteRepository asignacionClienteRepository;

    @Autowired
    private AsignacionClienteMapper asignacionClienteMapper;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private PatioAutosService patioAutosService;

    @Override
    @Transactional(readOnly = true)
    public AsignacionCliente getAsignacionCliente(Long id) {
        var cliente = asignacionClienteRepository.findById(id);
        if(cliente.isPresent()) return cliente.get();
        else throw new AsignacionClienteNotFoundException();
    }

    @Override
    @Transactional(readOnly = true)
    public AsignacionClienteResponseDto getAsignacionClienteAsResponse(Long id) {
        return asignacionClienteMapper.modelToResponseDTO(getAsignacionCliente(id));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public AsignacionClienteResponseDto createAsignacionCliente(AsignacionClienteRequestDto requestDto) {
        var asignacionCliente = asignacionClienteMapper.requestDtoToModel(requestDto);
        asignacionClienteRepository.save(asignacionCliente);
        return asignacionClienteMapper.modelToResponseDTO(asignacionCliente);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public AsignacionClienteResponseDto modifyAsignacionCliente(AsignacionClienteRequestDto requestDto, Long id) {
//        var asignacion = getAsignacionCliente(id);
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAsignacionCliente(Long id) {
        asignacionClienteRepository.delete(getAsignacionCliente(id));
    }
}
