package com.bancopichincha.credito.automotriz.components;

import com.bancopichincha.credito.automotriz.exception.EjecutivoRepetidoException;
import com.bancopichincha.credito.automotriz.service.ClienteService;
import com.bancopichincha.credito.automotriz.service.InitialDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class FileLoaderComponent {

    @Qualifier("cliente_service")
    @Autowired
    private InitialDataService clienteService;

    @Qualifier("ejecutivo_service")
    @Autowired
    private InitialDataService ejecutivoService;

    @Qualifier("marca_service")
    @Autowired
    private InitialDataService marcaService;

    @PostConstruct
    public void initDatabase() throws EjecutivoRepetidoException {
        clienteService.loadData();
        ejecutivoService.loadData();
        marcaService.loadData();
    }

}
