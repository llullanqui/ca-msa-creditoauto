package com.bancopichincha.credito.automotriz.mapper;

import com.bancopichincha.credito.automotriz.dto.ClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.ClienteResponseDto;
import com.bancopichincha.credito.automotriz.model.Cliente;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface ClienteMapper {

    ClienteResponseDto modelToResponseDTO(Cliente cliente);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sujetoCredito", ignore = true)
    Cliente requestDtoToModel(ClienteRequestDto cliente);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sujetoCredito", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateClienteFromDto(ClienteRequestDto dto, @MappingTarget Cliente cliente);

}
