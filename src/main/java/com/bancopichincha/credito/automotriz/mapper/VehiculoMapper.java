package com.bancopichincha.credito.automotriz.mapper;

import com.bancopichincha.credito.automotriz.dto.VehiculoRequestDto;
import com.bancopichincha.credito.automotriz.dto.VehiculoResponseDto;
import com.bancopichincha.credito.automotriz.model.Vehiculo;
import com.bancopichincha.credito.automotriz.service.MarcaService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Mapper(componentModel = "spring")
public abstract class VehiculoMapper {

    @Autowired
    @Qualifier("marca_service")
    protected MarcaService marcaService;

    public abstract VehiculoResponseDto modelToResponseDto(Vehiculo patioAutos);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "marca", expression = "java(marcaService.getMarca(requestDto.getMarca()))")
    public abstract Vehiculo requestDtoToModel(VehiculoRequestDto requestDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "marca", expression = "java(marcaService.getMarca(requestDto.getMarca()))")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract void updateVehiculoFromDto(VehiculoRequestDto requestDto, @MappingTarget Vehiculo patioAutos);

}
