package com.bancopichincha.credito.automotriz.mapper;

import com.bancopichincha.credito.automotriz.dto.AsignacionClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.AsignacionClienteResponseDto;
import com.bancopichincha.credito.automotriz.model.AsignacionCliente;
import com.bancopichincha.credito.automotriz.service.ClienteService;
import com.bancopichincha.credito.automotriz.service.PatioAutosService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Mapper(componentModel = "spring")
public abstract class AsignacionClienteMapper {

    @Autowired
    @Qualifier("cliente_service")
    protected ClienteService clienteService;

    @Autowired
    @Qualifier("patio_autos_service")
    protected PatioAutosService patioAutosService;

    public abstract AsignacionClienteResponseDto modelToResponseDTO(AsignacionCliente asignacionCliente);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "cliente", expression = "java(clienteService.getCliente(asignacionClienteRequestDto.getCliente()))")
    @Mapping(target = "patio", expression = "java(patioAutosService.getPatioAutos(asignacionClienteRequestDto.getPatio()))")
    @Mapping(target = "fechaAsignacion", ignore = true)
    public abstract AsignacionCliente requestDtoToModel(AsignacionClienteRequestDto asignacionClienteRequestDto);

}
