package com.bancopichincha.credito.automotriz.mapper;

import com.bancopichincha.credito.automotriz.dto.PatioAutosRequestDto;
import com.bancopichincha.credito.automotriz.dto.PatioAutosResponseDto;
import com.bancopichincha.credito.automotriz.model.PatioAutos;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface PatioAutosMapper {

    PatioAutosResponseDto modelToResponseDto(PatioAutos patioAutos);

    @Mapping(target = "id", ignore = true)
    PatioAutos requestDtoToModel(PatioAutosRequestDto requestDto);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updatePatioAutosFromDto(PatioAutosRequestDto dto, @MappingTarget PatioAutos patioAutos);

}
