package com.bancopichincha.credito.automotriz.utils;

import com.bancopichincha.credito.automotriz.model.Cliente;
import com.bancopichincha.credito.automotriz.model.Ejecutivo;
import com.bancopichincha.credito.automotriz.model.Marca;

import java.sql.Timestamp;

public class ArrayConverts {

    public static Cliente buildClientFromArray(String[] array) {
        return Cliente.builder()
                .identificacion(array[1])
                .nombres(array[2])
                .apellidos(array[3])
                .edad(Integer.parseInt(array[4]))
                .fechaNacimiento(Timestamp.valueOf(array[5]))
                .direccion(array[6])
                .telefono(array[7])
                .estadoCivil(array[8])
                .identificacionConyugue(array[9])
                .nombreConyugue(array[10])
                .sujetoCredito(array[11])
                .build();
    }

    public static Marca buildMarcaFromArray(String[] array) {
        return Marca.builder()
                .nombreMarca(array[1])
                .build();
    }

    public static Ejecutivo buildEjecutivoFromArray(String[] array) {
        return Ejecutivo.builder()
                .identificacion(array[1])
                .nombres(array[2])
                .apellidos(array[3])
                .direccion(array[4])
                .telefonoConvencional(array[5])
                .celular(array[6])
                .patio(array[7])
                .edad(Integer.parseInt(array[8]))
                .build();
    }
}
