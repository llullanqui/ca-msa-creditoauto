package com.bancopichincha.credito.automotriz.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "SOLICITUD_CREDITO")
@Table(name = "SOLICITUD_CREDITO")
@EqualsAndHashCode
public class SolicitudCredito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="SOLICITUD_CREDITO_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @JoinColumn(name="SOLICITUD_CREDITO_CLIENTE", nullable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;

    @JoinColumn(name="SOLICITUD_CREDITO_PATIO_AUTOS", nullable = false)
    @ManyToOne(optional = false)
    private PatioAutos patio;

    @JoinColumn(name="SOLICITUD_CREDITO_VEHICULO", nullable = false)
    @ManyToOne(optional = false)
    private Vehiculo vehiculo;

    @Column(name="SOLICITUD_CREDITO_MESES_PLAZO", nullable = false)
    private int mesesPlazo;

    @Column(name="SOLICITUD_CREDITO_CUOTAS", nullable = false)
    private BigDecimal cuotas;

    @Column(name="SOLICITUD_CREDITO_ENTRADA", nullable = false)
    private BigDecimal entrada;

    @JoinColumn(name="SOLICITUD_CREDITO_EJECUTIVO_VENTAS", nullable = false)
    @ManyToOne(optional = false)
    private Ejecutivo ejecutivoVentas;

    @Column(name="SOLICITUD_CREDITO_OBSERVACION", columnDefinition = "")
    @CreatedDate
    private String observacion;

}
