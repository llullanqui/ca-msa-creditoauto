package com.bancopichincha.credito.automotriz.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "VEHICULO")
@Table(name = "VEHICULO")
@EqualsAndHashCode
public class Vehiculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="VEHICULO_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column(name="VEHICULO_PLACA", nullable = false, unique = true, length = 10)
    private String placa;

    @Column(name="VEHICULO_MODELO", nullable = false, length = 10)
    private String modelo;

    @Column(name="VEHICULO_NUMERO_CHASIS", nullable = false)
    private String numeroChasis;

    @ManyToOne
    @JoinColumn(name="VEHICULO_MARCA", nullable = false)
    private Marca marca;

    @Column(name="VEHICULO_TIPO")
    private String tipo;

    @Column(name="VEHICULO_CILINDRAJE", nullable = false)
    private String cilindraje;

    @Column(name="VEHICULO_AVALUO", nullable = false)
    private BigDecimal avaluo;

}
