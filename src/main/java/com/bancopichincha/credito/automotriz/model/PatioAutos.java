package com.bancopichincha.credito.automotriz.model;

import lombok.*;

import javax.persistence.*;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "PATIO_AUTOS")
@Table(name = "PATIO_AUTOS")
@EqualsAndHashCode
public class PatioAutos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="PATIO_AUTOS_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column(name="PATIO_AUTOS_NOMBRE", nullable = false, length = 100, unique = true)
    private String nombre;

    @Column(name="PATIO_AUTOS_DIRECCION", nullable = false)
    private String direccion;

    @Column(name="PATIO_AUTOS_TELEFONO", nullable = false)
    private String telefono;

    @Column(name="PATIO_AUTOS_NUM_PUNTOS_VENTA", nullable = false, columnDefinition = "integer default 0")
    private int numeroPuntosVenta;

}
