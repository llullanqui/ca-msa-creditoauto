package com.bancopichincha.credito.automotriz.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "CLIENTE")
@Table(name = "CLIENTE")
@EqualsAndHashCode
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="CLIENTE_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column(name="CLIENTE_IDENTIFICACION", nullable = false, length = 10, unique = true)
    private String identificacion;

    @Column(name="CLIENTE_NOMBRES", nullable = false, length = 100)
    private String nombres;

    @Column(name="CLIENTE_APELLIDOS", nullable = false, length = 100)
    private String apellidos;

    @Column(name="CLIENTE_EDAD", nullable = false)
    private int edad;

    @Column(name="CLIENTE_FECHA_NACIMIENTO", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

    @Column(name="CLIENTE_DIRECCION", nullable = false)
    private String direccion;

    @Column(name="CLIENTE_TELEFONO", nullable = false)
    private String telefono;

    @Column(name="CLIENTE_ESTADO_CIVIL", nullable = false)
    private String estadoCivil;

    @Column(name="CLIENTE_CONYUGUE_IDENTIFICACION")
    private String identificacionConyugue;

    @Column(name="CLIENTE_CONYUGUE_NOMBRE")
    private String nombreConyugue;

    @Column(name="CLIENTE_SUJETO_CREDITO", nullable = false)
    private String sujetoCredito;

}
