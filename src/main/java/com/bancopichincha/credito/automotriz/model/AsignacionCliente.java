package com.bancopichincha.credito.automotriz.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "ASIGNACION_CLIENTE")
@Table(name = "ASIGNACION_CLIENTE", uniqueConstraints = {
        @UniqueConstraint(
                name = "not_same_assignation",
                columnNames = {"ASIGNACION_CLIENTE_FK_CLIENTE", "ASIGNACION_CLIENTE_FK_PATIO"}
        )
})
@EqualsAndHashCode
public class AsignacionCliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ASIGNACION_CLIENTE_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name="ASIGNACION_CLIENTE_FK_CLIENTE", nullable = false, updatable = false)
    private Cliente cliente;

    @ManyToOne(optional = false)
    @JoinColumn(name="ASIGNACION_CLIENTE_FK_PATIO", nullable = false)
    private PatioAutos patio;

    @CreationTimestamp
    @Column(name="ASIGNACION_CLIENTE_FECHA_ASIGNACION", columnDefinition = "timestamp default CURRENT_TIMESTAMP")
    private Date fechaAsignacion;

}
