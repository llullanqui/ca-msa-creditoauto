package com.bancopichincha.credito.automotriz.model;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "EJECUTIVO")
@Table(name = "EJECUTIVO")
@EqualsAndHashCode
public class Ejecutivo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="EJECUTIVO_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column(name="EJECUTIVO_IDENTIFICACION", nullable = false, length = 10, unique = true)
    private String identificacion;

    @Column(name="EJECUTIVO_NOMBRES", nullable = false, length = 100)
    private String nombres;

    @Column(name="EJECUTIVO_APELLIDOS", nullable = false, length = 100)
    private String apellidos;

    @Column(name="EJECUTIVO_DIRECCION", nullable = false)
    private String direccion;

    @Column(name="EJECUTIVO_TELEFONO", nullable = false)
    private String telefonoConvencional;

    @Column(name="EJECUTIVO_CELULAR", nullable = false)
    private String celular;

    @Column(name="EJECUTIVO_PATIO", nullable = false)
    private String patio;

    @Column(name="EJECUTIVO_EDAD", nullable = false)
    private int edad;

}
