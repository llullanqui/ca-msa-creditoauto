package com.bancopichincha.credito.automotriz.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "MARCA")
@Table(name = "MARCA")
@EqualsAndHashCode
public class Marca {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="MARCA_ID", columnDefinition = "serial")
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column(name="MARCA_NOMBRE", nullable = false, length = 100, unique = true)
    private String nombreMarca;

}
