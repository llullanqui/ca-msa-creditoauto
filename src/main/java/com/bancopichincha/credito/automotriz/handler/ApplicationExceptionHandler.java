package com.bancopichincha.credito.automotriz.handler;

import com.bancopichincha.credito.automotriz.dto.ApplicationError;
import com.bancopichincha.credito.automotriz.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ClienteNotFoundException.class})
    public ResponseEntity<ApplicationError> handleException(ClienteNotFoundException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Cliente no encontrado.")
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(value = {EjecutivoRepetidoException.class})
    public ResponseEntity<ApplicationError> handleException(EjecutivoRepetidoException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Ejecutivo repetido.")
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(value = {MarcaNotFoundException.class})
    public ResponseEntity<ApplicationError> handleException(MarcaNotFoundException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Marca no encontrada.")
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(value = {PatioAutosNotFoundException.class})
    public ResponseEntity<ApplicationError> handleException(PatioAutosNotFoundException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Patio no encontrado.")
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(value = {VehiculoNotFoundException.class})
    public ResponseEntity<ApplicationError> handleException(VehiculoNotFoundException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Vehiculo no encontrado.")
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(value = {AsignacionClienteNotFoundException.class})
    public ResponseEntity<ApplicationError> handleException(AsignacionClienteNotFoundException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Asignacion no encontrado.")
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(value = {SQLException.class})
    public ResponseEntity<ApplicationError> handleException(SQLException e) {
        var error = ApplicationError.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message(e.getLocalizedMessage())
                .build();
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

}
