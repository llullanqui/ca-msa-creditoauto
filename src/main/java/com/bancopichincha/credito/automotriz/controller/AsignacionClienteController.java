package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.dto.AsignacionClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.AsignacionClienteResponseDto;
import com.bancopichincha.credito.automotriz.service.AsignacionClienteService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/asignacion_cliente")
public class AsignacionClienteController {

    @Qualifier("asignacion_cliente_service")
    private final AsignacionClienteService asignacionClienteService;

    public AsignacionClienteController(AsignacionClienteService asignacionClienteService) {
        this.asignacionClienteService = asignacionClienteService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<AsignacionClienteResponseDto> getAsignacion(@PathVariable Long id) {
        return new ResponseEntity<>(asignacionClienteService.getAsignacionClienteAsResponse(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AsignacionClienteResponseDto> createAsignacion(@RequestBody AsignacionClienteRequestDto request) {
        return new ResponseEntity<>(asignacionClienteService.createAsignacionCliente(request), HttpStatus.OK);
    }

}
