package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.dto.VehiculoRequestDto;
import com.bancopichincha.credito.automotriz.dto.VehiculoResponseDto;
import com.bancopichincha.credito.automotriz.service.VehiculoService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehiculo")
public class VehiculoController {

    @Qualifier("vehiculo_service")
    private final VehiculoService vehiculoService;

    public VehiculoController(VehiculoService vehiculoService) {
        this.vehiculoService = vehiculoService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<VehiculoResponseDto> getVehiculo(@PathVariable Long id) {
        return new ResponseEntity<>(vehiculoService.getVehiculoAsResponse(id), HttpStatus.OK);
    }

    @GetMapping("/busqueda")
    @ResponseBody
    public List<VehiculoResponseDto> getVehiculoByParams(@RequestParam(name = "marca") Long marca, @RequestParam(name = "modelo") String modelo){
        return vehiculoService.getVehiculoByMarcaAndModelo(marca, modelo);
    }

    @PostMapping
    public ResponseEntity<VehiculoResponseDto> createCliente(@RequestBody VehiculoRequestDto request) {
        return new ResponseEntity<>(vehiculoService.createVehiculo(request), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<VehiculoResponseDto> modifyCliente(@RequestBody VehiculoRequestDto request, @PathVariable Long id) {
        return new ResponseEntity<>(vehiculoService.modifyVehiculo(request, id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable Long id) {
        vehiculoService.deleteVehiculo(id);
        return ResponseEntity.ok().build();
    }

}
