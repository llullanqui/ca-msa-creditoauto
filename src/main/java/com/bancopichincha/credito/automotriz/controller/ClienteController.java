package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.dto.ClienteRequestDto;
import com.bancopichincha.credito.automotriz.dto.ClienteResponseDto;
import com.bancopichincha.credito.automotriz.mapper.ClienteMapper;
import com.bancopichincha.credito.automotriz.service.ClienteService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Qualifier("cliente_service")
    private final ClienteService clienteService;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClienteResponseDto> getCliente(@PathVariable Long id) {
        return new ResponseEntity<>(clienteService.getClienteAsResponse(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ClienteResponseDto> createCliente(@RequestBody ClienteRequestDto request) {
        return new ResponseEntity<>(clienteService.createCliente(request), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ClienteResponseDto> modifyCliente(@RequestBody ClienteRequestDto request, @PathVariable Long id) {
        return new ResponseEntity<>(clienteService.modifyCliente(request, id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable Long id) {
        clienteService.deleteCliente(id);
        return ResponseEntity.ok().build();
    }

}
