package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.dto.PatioAutosRequestDto;
import com.bancopichincha.credito.automotriz.dto.PatioAutosResponseDto;
import com.bancopichincha.credito.automotriz.service.PatioAutosService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patio_autos")
public class PatioAutosController {

    @Qualifier("patio_autos_service")
    private final PatioAutosService patioAutosService;

    public PatioAutosController(PatioAutosService patioAutosService) {
        this.patioAutosService = patioAutosService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PatioAutosResponseDto> getPatioAutos(@PathVariable Long id) {
        return new ResponseEntity<>(patioAutosService.getPatioAutosAsResponse(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PatioAutosResponseDto> createCliente(@RequestBody PatioAutosRequestDto request) {
        return new ResponseEntity<>(patioAutosService.createPatioAutos(request), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<PatioAutosResponseDto> modifyCliente(@RequestBody PatioAutosRequestDto request, @PathVariable Long id) {
        return new ResponseEntity<>(patioAutosService.modifyPatioAutos(request, id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable Long id) {
        patioAutosService.deletePatioAutos(id);
        return ResponseEntity.ok().build();
    }

}
