package com.bancopichincha.credito.automotriz;

import com.bancopichincha.credito.automotriz.exception.EjecutivoRepetidoException;
import com.bancopichincha.credito.automotriz.model.Cliente;
import com.bancopichincha.credito.automotriz.model.Ejecutivo;
import com.bancopichincha.credito.automotriz.model.Marca;
import com.bancopichincha.credito.automotriz.repository.ClienteRepository;
import com.bancopichincha.credito.automotriz.repository.EjecutivoRepository;
import com.bancopichincha.credito.automotriz.repository.MarcaRepository;
import com.bancopichincha.credito.automotriz.utils.ArrayConverts;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@EntityScan("com.bancopichincha.credito.automotriz.model")
public class CaMsaCreditoautoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaMsaCreditoautoApplication.class, args);
	}

}
